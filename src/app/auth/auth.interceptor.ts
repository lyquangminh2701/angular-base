import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {    
        const currentUser = JSON.parse(JSON.parse(JSON.stringify(localStorage.getItem('currentUser'))));
        if (currentUser === null){
            return next.handle(req.clone({}));
        }
        
        return next.handle(req.clone({setHeaders: {
            Authorization: currentUser.token
        }}));
    }
}
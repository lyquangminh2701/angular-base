import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Route, Router, RouterStateSnapshot, UrlTree, CanMatch, UrlSegment } from '@angular/router';
import { Store } from '@ngrx/store';
import { map, Observable } from 'rxjs';
import { selectAccount_isLoggedIn, selectAccount } from '../store/selectors/account.selector';
import { AccountState } from '../store/reducers/account.reducer';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  private isLoggedIn!: boolean;
  constructor(private router: Router, private store: Store<{account: AccountState}>) {
    this.store.select(selectAccount_isLoggedIn).pipe(
      map((isLoggedIn) => {
        this.isLoggedIn = isLoggedIn
      })
    ).subscribe();
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.log('AuthGuard#canActivate called');
    
    return this.checkLogin();
  }

  checkLogin(): true | UrlTree {
    if (this.isLoggedIn){
      return true;
    }
    // this.authService.redirectUrl = url;
    return this.router.parseUrl('/login');
  }
}

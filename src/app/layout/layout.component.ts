import { Component, OnInit } from '@angular/core';

export interface MenuSidebar {
  title: string,
  iconName: string,
  path: string,
}

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})

export class LayoutComponent implements OnInit {
  menu: MenuSidebar[];
  constructor() {
    this.menu = [
      {
        title: 'DashBoard',
        iconName: 'dashboard',
        path: '/'
      },
      {
        title: 'Đơn hàng',
        iconName: 'dashboard',
        path: '/order',
      },
      {
        title: 'Nhân viên',
        iconName: 'dashboard',
        path: '/account',
      },
      {
        title: 'Danh mục',
        iconName: 'dashboard',
        path: '/category'
      },
      {
        title: 'Sản phẩm',
        iconName: 'dashboard',
        path: '/product'
      }
    ]
  }

  ngOnInit(): void {
 
  }

}

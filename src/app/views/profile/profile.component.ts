import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as moment from 'moment';
import { map, Observable, of, tap } from 'rxjs';
import { ValidatorFn } from '@angular/forms';
import { editUser, updateUserRequested } from 'src/app/store/actions/user.action';
import { AccountState } from 'src/app/store/reducers/account.reducer';
import { selectAccount_currentUser } from 'src/app/store/selectors/account.selector';
// import { selectAccount_currentUser } from 'src/app/store/selectors/account.selector';


function birthValidator(control: FormControl): { [key: string]: boolean}{
  if (moment(control.value).format('YYYYMMDD').toString() > moment().format('YYYYMMDD').toString()){
    return {
      invalidBirth: true
    }
  }
  return {};
}

function comparePassword(value: string): ValidatorFn{
  return (c: AbstractControl): ValidationErrors | null  => {
    console.log("vadialte password")
    if (c === null || c.value === null)
      return null;
    console.log(c.value, " ", value);
    if (c.value === value)
      return null;
    
    return {
      matchPassword: true
    }
  }
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfilePageComponent implements OnInit {
    form!: FormGroup;
    passwordChangeForm!: FormGroup;
    constructor(
      private formBuilder: FormBuilder, 
      private store: Store<{account: AccountState}>, 
    ) {
      console.log("EditAccoutComponent Created");
      this.store.select(selectAccount_currentUser).pipe(
        map(state => {
          this.form = this.formBuilder.group({
            idUser: [ state?.idUser, Validators.compose([Validators.required])],
            name: [ state?.name, Validators.compose([Validators.required])],
            phone: [ state?.phone, Validators.compose([Validators.required, Validators.pattern(/^(0|\+84)(\s|\.)?((3[2-9])|(5[689])|(7[06-9])|(8[1-689])|(9[0-46-9]))(\d)(\s|\.)?(\d{3})(\s|\.)?(\d{3})$/)])],
            birth: [ state?.birth, Validators.compose([Validators.required, birthValidator])],
            email: [ state?.email ],
          });
        })
      ).subscribe();
      this.form.controls['email'].disable();
      this.passwordChangeForm = formBuilder.group({
        currentPassword: ['Minh', Validators.compose([Validators.required])],
        newPassword: ['', Validators.compose([Validators.required, Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/)])],
        renewPassword: ['Quang', Validators.compose([Validators.required, comparePassword('aaa')])],
      })
    }
  
    onSubmit(): void{
      if (this.form.invalid)
      return;
      this.store.dispatch(updateUserRequested({user: this.form.value}));
    }
  
    ngOnInit(): void {
      this.passwordChangeForm.valueChanges.pipe(
        tap((values) => {
        })
      ).subscribe();
    }
  
    // Validator form 
    getNameErrorMessage(){
      if (this.form.controls['name'].hasError('required'))
        return 'Họ tên không được trống.';
      return '';
    }
  
    getPhoneErrorMessage(){
      if (this.form.controls['phone'].hasError('required'))
        return 'Số điện thoại không được trống';
      if (this.form.controls['phone'].hasError('pattern'))
        return 'Số điện thoại không hợp lệ.';
      return '';
    }
  
    getBirthErrorMessage(){
      if (this.form.controls['birth'].hasError('required'))
        return 'Ngày sinh không để trống.';
      if (this.form.controls['birth'].hasError('invalidBirth'))
        return 'Ngày sinh không vượt quá ngày hiện tại';
      return '';
    }
    getMailErrorMessage(){
      if (this.form.controls['email'].hasError('required'))
        return 'Mail không được trống.';
      if (this.form.controls['email'].hasError('email'))
        return 'Mail không hợp lệ.';
        return '';
    }

    // Validate form password change
    getNewPasswordMessage(){
      if (this.passwordChangeForm.controls['newPassword'].hasError('required'))
        return 'Mật khẩu không được trống';
      if (this.passwordChangeForm.controls['newPassword'].hasError('pattern'))
        return "Mật khẩu phải tối thiểu 8 ký tự, ít nhất một chữ hoa, một chữ thường và một số";
      return '';
    }
  }
  
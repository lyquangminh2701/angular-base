import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as moment from 'moment';
import { Observable, of, Subscription } from 'rxjs';
import { editUser, updateUserRequested } from 'src/app/store/actions/user.action';
import { User } from 'src/app/store/models/user.model';
import Swal from 'sweetalert2';

function birthValidator(control: FormControl): { [key: string]: boolean}{
  if (moment(control.value).format('YYYYMMDD').toString() > moment().format('YYYYMMDD').toString()){
    return {
      invalidBirth: true
    }
  }
  return {};
}

@Component({
  selector: 'app-account-edit',
  templateUrl: './account-edit.component.html',
  styleUrls: ['./account-edit.component.css']
})
export class AccountEditComponent implements OnInit {
  form: FormGroup;
  idParam: number;
  constructor(
    private formBuilder: FormBuilder, 
    private store: Store<{user: User[]}>, 
    private router: Router,
    private activatedRouter: ActivatedRoute
  ) {
    this.idParam = -1;
    this.form = this.formBuilder.group({
      idUser: [window.history.state.idUser, Validators.compose([Validators.required])],
      name: [window.history.state.name, Validators.compose([Validators.required])],
      phone: [window.history.state.phone, Validators.compose([Validators.required, Validators.pattern(/^(0|\+84)(\s|\.)?((3[2-9])|(5[689])|(7[06-9])|(8[1-689])|(9[0-46-9]))(\d)(\s|\.)?(\d{3})(\s|\.)?(\d{3})$/)])],
      birth: [window.history.state.birth, Validators.compose([Validators.required, birthValidator])],
      email: [window.history.state.email],
      gender: [window.history.state.gender, Validators.compose([Validators.required])]
    });
    this.form.controls['email'].disable();
  }

  onSubmit(): void{
    if (this.form.invalid)
      return;
    console.log(this.form.value);
    this.store.dispatch(updateUserRequested({user: this.form.value}));
  }

  ngOnInit(): void {

  }

  // Validator
  getNameErrorMessage(){
    if (this.form.controls['name'].hasError('required'))
      return 'Họ tên không được trống.';
    return '';
  }

  getPhoneErrorMessage(){
    if (this.form.controls['phone'].hasError('required'))
      return 'Số điện thoại không được trống';
    if (this.form.controls['phone'].hasError('pattern'))
      return 'Số điện thoại không hợp lệ.';
    return '';
  }

  getBirthErrorMessage(){
    if (this.form.controls['birth'].hasError('required'))
      return 'Ngày sinh không để trống.';
    if (this.form.controls['birth'].hasError('invalidBirth'))
      return 'Ngày sinh không vượt quá ngày hiện tại';
    return '';
  }
  getMailErrorMessage(){
    if (this.form.controls['email'].hasError('required'))
      return 'Mail không được trống.';
    if (this.form.controls['email'].hasError('email'))
      return 'Mail không hợp lệ.';
      return '';
  }
  getGenderErrorMessage(){
    if (this.form.controls['gender'].hasError('required'))
      return 'Giới tính không được trống.';
    return '';
  }
}

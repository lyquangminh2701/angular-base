import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { User } from 'src/app/store/models/user.model';
import { addUser, createUserRequested } from 'src/app/store/actions/user.action';
import { map, Observable, of, Subscription, take, takeUntil, tap } from 'rxjs';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import * as moment from 'moment';

function birthValidator(control: FormControl): { [key: string]: boolean}{
  if (moment(control.value).format('YYYYMMDD').toString() > moment().format('YYYYMMDD').toString()){
    return {
      invalidBirth: true
    }
  }
  return {};
}

@Component({
  selector: 'app-account-create',
  templateUrl: './account-create.component.html',
  styleUrls: ['./account-create.component.css']
})
export class AccountCreateComponent implements OnInit {
  form: FormGroup;
  isBtnPending: boolean;
  constructor(private formBuilder: FormBuilder, private store: Store<{user: User[]}>, private router: Router) {
    this.form = formBuilder.group({
      id: Math.floor(Math.random() * 100),
      name: ['', Validators.compose([Validators.required])],
      phone: ['', Validators.compose([Validators.required, Validators.pattern(/^(0|\+84)(\s|\.)?((3[2-9])|(5[689])|(7[06-9])|(8[1-689])|(9[0-46-9]))(\d)(\s|\.)?(\d{3})(\s|\.)?(\d{3})$/)])],
      birth: ['', Validators.compose([Validators.required, birthValidator])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      gender: ['', Validators.compose([Validators.required])],
    });
    this.isBtnPending = true;
    const subscription = this.store.select('user').pipe(
      tap((state : any) =>{
        this.isBtnPending = state.isCreateLoading;
      })
    ).subscribe();
  }

  onSubmit(): void{
    if (this.form.invalid){
      return;
    }
    this.store.dispatch(createUserRequested({user: this.form.value}));
  }

  ngOnInit(): void {
  }

  // Validator
  getNameErrorMessage(){
    if (this.form.controls['name'].hasError('required'))
      return 'Họ tên không được trống.';
    return '';
  }

  getPhoneErrorMessage(){
    if (this.form.controls['phone'].hasError('required'))
      return 'Số điện thoại không được trống.';
    if (this.form.controls['phone'].hasError('pattern'))
      return 'Số điện thoại không hợp lệ.';
    return '';
  }

  getBirthErrorMessage(){
    if (this.form.controls['birth'].hasError('required'))
      return 'Ngày sinh không để trống.';
    if (this.form.controls['birth'].hasError('invalidBirth'))
      return 'Ngày sinh không vượt quá ngày hiện tại';
    return '';
  }
  getMailErrorMessage(){
    if (this.form.controls['email'].hasError('required'))
      return 'Mail không được trống.';
    if (this.form.controls['email'].hasError('email'))
      return 'Mail không hợp lệ.';
      return '';
  }
  getGenderErrorMessage(){
    if (this.form.controls['gender'].hasError('required'))
      return 'Giới tính không được trống.';
    return '';
  }
}

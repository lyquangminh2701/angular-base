import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { map } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { authenticateTokenRequest, loginRequested } from 'src/app/store/actions/auth.action';
import { Account } from 'src/app/store/models/account.model';
import { AccountState } from 'src/app/store/reducers/account.reducer';
import { selectAccount_isLoggedIn } from 'src/app/store/selectors/account.selector';

export interface formRequest{
  username: string,
  password: string
}

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  form: FormGroup;
  constructor(private formBuilder: FormBuilder, private authService: AuthService,private router: Router, private store: Store<{account: AccountState}>) {
    this.form = formBuilder.group({
      username: 'admin@gmail.com',
      password: 'admin@gmail.com'
    })
  }

  login(): void{
    const payload: Account = this.form.value;
    this.store.dispatch(loginRequested({account: payload}));
  }

  ngOnInit(): void {
    console.log("login component ngOnInit");
    const currentUser = localStorage.getItem('currentUser');
    if ( currentUser !== null){
      console.log('call authenticate token request');
      this.store.dispatch(authenticateTokenRequest());
    }
  }

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { exhaustMap, filter, first, map, mergeMap, Observable, of, switchMap, take, tap } from 'rxjs';
import { User } from 'src/app/store/models/user.model';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { deleteUserRequested, getUserRequested } from 'src/app/store/actions/user.action';
interface deleteAction {
  isConfirm: boolean;
  id: number;
}

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})

export class AccountComponent implements OnInit {
  displayedColumns: string[] = ['idUser', 'name', 'gender' , 'birth', 'phone', 'email', 'action'];
  user$: Observable<User[]>;
  dataTable: User[] = [];
  isLoading: boolean = false;

  constructor(private store: Store<{user: User[]}>) {
    this.user$ = store.select('user');
    this.user$.pipe( // return subcription 
      tap((state: any) => {
        this.dataTable = state.users;
        this.isLoading = state.isLoading;
      })
    ).subscribe();
    console.log("Account componet created!")
  }

  onDelete(id: number): void{
    Swal.fire({
      title: 'Are you sure want to remove?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.store.dispatch(deleteUserRequested({id}));
      }
    })
  }

  formatDatetime(date: string): string{
    return moment(date).format('DD/MM/YYYY');
  }

  ngOnInit(): void {
    this.store.dispatch(getUserRequested())
  }
}

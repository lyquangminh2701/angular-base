import { NgModule } from "@angular/core";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule} from '@ngrx/store-devtools';
import { environment } from "src/environments/environment";


import { userReducer } from "./store/reducers/user.reducer";
import { UserEffects } from "./store/effects/user.effect";
import { accountReducer } from "./store/reducers/account.reducer";
import { AccountEffects } from "./store/effects/account.effect";
@NgModule({
    imports:[
        StoreModule.forRoot({
            account: accountReducer,
            user: userReducer,
            
        }),
        EffectsModule.forRoot([
            UserEffects,
            AccountEffects
        ]),
        StoreDevtoolsModule.instrument({
            maxAge: 25, // Retains last 25 states
            logOnly: environment.production, // Restrict extension to log-only mode
            autoPause: true, // Pauses recording actions and state changes when the extension window is not open
          }),
    ],
    exports:[
        StoreModule
    ]
})
export class AppStoreModule { };
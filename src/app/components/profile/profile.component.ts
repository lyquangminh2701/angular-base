import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AuthService } from 'src/app/services/auth.service';
// import { AuthService } from 'src/app/auth/auth.service';
import { AccountState } from 'src/app/store/reducers/account.reducer';
import { selectAccount_currentUser_name } from 'src/app/store/selectors/account.selector';

@Component({
  selector: 'app-profile-component',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  name!: string | undefined;
  constructor(private router: Router, private store:Store<{account: AccountState}>, private authService: AuthService) { }

  logout(): void{
    this.authService.logout().subscribe(()=>{
      this.router.navigate(['login']);
    })
  }

  ngOnInit(): void {
    this.store.select(selectAccount_currentUser_name).subscribe(value => {
      this.name = value;
    })
  }

}

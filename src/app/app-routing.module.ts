import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountComponent } from './views/account/account.component';
import { PagenotfoundComponent } from './views/pagenotfound/pagenotfound.component';
import { AccountCreateComponent } from './views/account-create/account-create.component';
import { AccountEditComponent } from './views/account-edit/account-edit.component';
import { AuthGuard } from './auth/auth.guard';
import { LayoutComponent } from './layout/layout.component';
import { AuthComponent } from './views/auth/auth.component';
import { ProfilePageComponent } from './views/profile/profile.component';
const routes: Routes = [
  {path:'', redirectTo: 'account', pathMatch:'full'},
  {path:'account', component: AccountComponent},
  {path:'account/create', component:AccountCreateComponent},
  {path:'account/edit/:id', component: AccountEditComponent},
  {path:'**', component: PagenotfoundComponent}
];

const routesv2: Routes = [
  {
    path: 'login',
    component: AuthComponent
  },
  {
    path: '', //admin
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { 
        path:'', 
        redirectTo: 'account', 
        pathMatch:'full'
      },
      {path:'account', component: AccountComponent},
      {path:'account/create', component:AccountCreateComponent},
      {path:'account/edit/:id', component: AccountEditComponent},
      {path:'profile', component: ProfilePageComponent},
    ]
  },
  {
    path:'**', 
    component: PagenotfoundComponent
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routesv2)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

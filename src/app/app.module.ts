import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { AppStoreModule } from './app-store.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppMaterialModule } from './app-material.module';
import { AppComponent } from './app.component';
import { AccountComponent } from './views/account/account.component';
import { PagenotfoundComponent } from './views/pagenotfound/pagenotfound.component';
import { AccountCreateComponent } from './views/account-create/account-create.component';
import { AccountEditComponent } from './views/account-edit/account-edit.component';
import { LayoutComponent } from './layout/layout.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AuthComponent } from './views/auth/auth.component';
import { ProfilePageComponent } from './views/profile/profile.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { AuthInterceptor } from './auth/auth.interceptor';
@NgModule({
  declarations: [
    AppComponent,
    AccountComponent,
    PagenotfoundComponent,
    AccountCreateComponent,
    AccountEditComponent,
    LayoutComponent,
    ProfileComponent,
    AuthComponent,
    ProfilePageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppMaterialModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppStoreModule,
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { createAction, props } from "@ngrx/store";
import { Account } from "../models/account.model";
import { AuthPayload } from "src/app/services/auth.service";
export const loginRequested = createAction(
    '[AUTH] USER LOGIN REQUESTED',
    props<{account:Account}>(),
)

export const loginSuccess = createAction(
    '[AUTH] USER LOGIN SUCCESS',
    props<AuthPayload>(),
)

export const loginError = createAction(
    '[AUTH] USER LOGIN ERROR',
)

export const logoutSuccess = createAction(
    '[AUTH] USER LOGOUT SUCCESS'
)

export const logout = createAction(
    '[AUTH] LOGOUT'
)

export const authenticateTokenRequest = createAction(
    '[AUTH] AUTHENTICATE TOKEN REQUEST'
)
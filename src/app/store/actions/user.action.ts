import { createAction, props } from "@ngrx/store";
import { User } from "../models/user.model";

export const addUser = createAction(
    '[USER] ADD LOADING',
    props<{user: User}>(),
)

export const editUser = createAction(
    '[User] Edit',
    props<{user: User}>(),
)

export const getUserFromLocalStorage = createAction(
    '[User] Get LocalStorage'
)

export const getUserRequested = createAction(
    '[USER] GET USER REQUESTED'
);
export const getUserSuccess = createAction(
    '[USER] GET USER SUCCESS',
    props<{users: User[]}>()
)
export const getUserError = createAction(
    '[USER] GET USER ERROR',
    props<{error: any}>()
)
// ---- DELETE USER --- //
export const deleteUserRequested = createAction(
    '[USER] DELETE USER REQUEST',
    props<{id: number}>(),
)

export const deleteUserSuccess = createAction(
    '[USER] DELETE USER SUCCESS',
)

export const deleteUserError = createAction(
    '[USER] DELETE USER ERROR'
)
// ---- CREATE USER --- //

export const createUserRequested = createAction(
    '[USER] CREATE USER REQUEST',
    props<{user: User}>(),
)

export const createUserSuccess = createAction(
    '[USER] CREATE USER SUCCESS',
)

export const createUserError = createAction(
    '[USER] CREATE USER ERROR'
)

// ---- UPDATE USER ---- //
export const updateUserRequested = createAction(
    '[USER] UPDATE USER REQUEST',
    props<{user: User}>(),
)

export const updateUserSuccess = createAction(
    '[USER] UPDATE USER SUCCESS',
)
export interface User{
    idUser: number,
    name: string,
    phone: string,
    birth: string,
    email: string
}
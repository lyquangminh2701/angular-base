import { createReducer, on } from "@ngrx/store";
import { loginRequested, loginSuccess, loginError, logoutSuccess } from "../actions/auth.action";
import { User } from "../models/user.model";

export interface AccountState{
    isLoading: boolean,
    isLoggedIn: boolean,
    currentUser: User | null,
    role: string | null,
} 

const initialState: AccountState = {
    isLoading: false,
    isLoggedIn: false,
    currentUser: null,
    role: null
}

export const accountReducer = createReducer(
    initialState,
    on(loginRequested, (state, action) =>{
        return {
            ...state,
            isLoading: true
        }
    }),
    on(loginSuccess, (state,action) =>{
        return {
            ...state,
            currentUser: {
                idUser: action.idUser,
                name : action.name,
                birth : action.birth, 
                phone : action.phone, 
                email : action.email,
            },
            isLoading: false,
            isLoggedIn: true,
            role: action.role,
        }
    }),
    on(loginError, (state, action) => {
        return {
            ...state,
            isLoading: false
        }
    }),
    on(loginRequested, (state, action) => {
        return {
            ...state,
            isLoading: true
        }
    }),
    on(logoutSuccess, (state, action) => {
        localStorage.removeItem('currentUser');
        return {
            ...state,
            isLoggedIn: false
        }
    })
)
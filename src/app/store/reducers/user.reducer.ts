import { createReducer, on } from "@ngrx/store";
import Swal from "sweetalert2";
import { addUser, editUser, getUserFromLocalStorage, getUserRequested ,getUserSuccess, getUserError, deleteUserError, createUserRequested, createUserSuccess, updateUserSuccess, createUserError } from "../actions/user.action";
import { User } from "../models/user.model";

export interface UserState {
    isLoading: boolean , 
    isCreateLoading: boolean,
    users: User[]
}

export const initialState: UserState  = {
    isLoading : false,
    isCreateLoading: false,
    users: [
        {idUser: 1, name: 'Quang Minh', birth: '2022-08-23',phone: '0123654987', email:'minh@gmail.com'},
        {idUser: 2, name: 'Quang Minh', birth: '2002-08-02',phone: '0123654987', email:'minh@gmail.com'},
        {idUser: 3, name: 'Quang Minh', birth: '2002-08-23',phone: '0123654987', email:'minh@gmail.com'},
        {idUser: 4, name: 'Quang Minh', birth: '2002-08-24',phone: '0123654987', email:'minh@gmail.com'},
        {idUser: 5, name: 'Quang Minh', birth: '2002-08-25',phone: '0123654987', email:'minh@gmail.com'},
        {idUser: 6, name: 'Quang Minh', birth: '2002-08-26',phone: '0123654987', email:'minh@gmail.com'},
        {idUser: 7, name: 'Quang Minh', birth: '2002-08-27',phone: '0123654987', email:'minh@gmail.com'}
    ]
}

export const userReducer = createReducer(
    initialState,
    on(addUser, (state, action)=> {
        return state;
    }),
    on(editUser, (state, action)=>{
        return state;
    }),
    on(getUserFromLocalStorage, (state, action)=>{
       const newState = JSON.parse(localStorage.getItem('user')|| '[]');
        return newState;
    }),
    on(getUserRequested, (state, action)=> {
        console.log("getUserRequested reducer");
        return {
            ...state,
            isLoading: true
        }
    }),
    on(getUserSuccess, (state, action) => {
        console.log("getUserSuccess reducer");
        return {
            ...state,
            users: action.users,
            isLoading: false
        }
    }),
    on(getUserError, (state, action)=> {
        console.log("getUserError reducer");
        return {
            ...state,
            isLoading: false,
            users: []
        }
    }),
    on(deleteUserError, (state, action) =>{
        Swal.fire({
            title: 'Delete Fail!',
            icon: 'error',
            text: "Server Error",
            confirmButtonText: 'OK!'
          })
        return state;
    }),
    on(createUserRequested, (state, action) => {
        return {
            ...state,
            isCreateLoading: true
        }
    }),
    on(createUserSuccess, (state, action) => {
        return {
            ...state,
            isCreateLoading: false
        }
    }),
    on(createUserError, (state, action) =>{
        return {
            ...state,
            isCreateLoading: false
        }
    }),
    on(updateUserSuccess, (state, action) =>{
        return state;
    })
)

import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { createEffect, Actions, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { catchError, EMPTY, map, mergeMap, of, tap } from "rxjs";
import { AuthService } from "src/app/services/auth.service";
import { loginRequested, loginSuccess, loginError, logout, authenticateTokenRequest } from "../actions/auth.action";

@Injectable()
export class AccountEffects {
    constructor(private action$: Actions, private store: Store, private authService: AuthService, private router: Router) {}

    loginAccountRequest$ = createEffect(() => this.action$.pipe(
        ofType(loginRequested),
        mergeMap(({account}) => this.authService.login(account).pipe(
            map((payload) => loginSuccess(payload)),
            tap((res)=> {
                localStorage.setItem('currentUser', JSON.stringify(res));
            }),
            tap(() => {
                this.router.navigateByUrl('/');
            }),
            catchError((error: any) => {
                return of(loginError());
            })
        ))
    ))
    
    authenticateTokenRequest$ = createEffect(() => this.action$.pipe(
        ofType(authenticateTokenRequest),
        mergeMap(() => this.authService.authenticate().pipe(
            map((payload) => loginSuccess(payload)),
            tap((res)=> {
                localStorage.setItem('currentUser', JSON.stringify(res));
            }),
            tap(() => {
                this.router.navigateByUrl('/');
            }),
            catchError(() => EMPTY)
        ))
    ))
}
import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { EMPTY, of } from "rxjs";
import { map, mergeMap, catchError, tap, take, switchMap} from 'rxjs/operators';
import { UsersService } from "src/app/services/users.service";
import { User } from "../models/user.model";
import { getUserRequested, getUserSuccess, getUserError, deleteUserRequested, deleteUserError, createUserRequested, createUserSuccess, createUserError, updateUserRequested, updateUserSuccess } from "../actions/user.action";
import Swal from "sweetalert2";
import { Router } from "@angular/router";
@Injectable()
export class UserEffects{
    constructor(private actions$: Actions, private userService: UsersService, private store: Store<{user: User[]}>, private router: Router) {}

    loadUser$ = createEffect(() => this.actions$.pipe(
        ofType(getUserRequested),
        mergeMap(()=> this.userService.getAll().pipe(
            map((users) => getUserSuccess({users})),
            catchError((error: any) => of(getUserError(error)))
        ))
    ));

    deleteUser$ = createEffect(() => this.actions$.pipe(
      ofType(deleteUserRequested),
      switchMap(({id}) => 
        this.userService.deleteOne(id).pipe(
            map(() => getUserRequested()),
            tap(() =>{
                Swal.fire({
                    title: 'Deleted Success!',
                    icon: 'success',
                    confirmButtonText: 'OK!'
                })
            }),
            catchError(() => {
                Swal.fire({
                    title: 'Delete Fail!',
                    icon: 'error',
                    text: "Server Error",
                    confirmButtonText: 'OK!'
                  })
                return EMPTY;
            }),
        )
      )
    ));

    createUser$ = createEffect(()=> this.actions$.pipe(
        ofType(createUserRequested),
        mergeMap((action) => this.userService.createOne(action.user).pipe(
            map((user)=> createUserSuccess()),
            tap(() =>{
                Swal.fire({
                    title: 'Create Success!',
                    icon: 'success',
                    confirmButtonText: 'OK!'
                })
                this.router.navigate(['/account']);
            }),
            catchError((res: any) => {
                Swal.fire({
                    title: 'Create Error!',
                    icon: 'error',
                    text: JSON.parse(JSON.stringify(res)).error,
                    confirmButtonText: 'OK!'
                })
                return of(createUserError());
            })
        )),
   ))

   updateUser$ = createEffect(() => this.actions$.pipe(
    ofType(updateUserRequested),
    mergeMap((action) => this.userService.updateOne(action.user.idUser, action.user).pipe(
        map((user) => updateUserSuccess()),
        tap(() =>{
            Swal.fire({
                title: 'Update Success!',
                icon: 'success',
                confirmButtonText: 'OK!'
            })
            this.router.navigate(['/account']);
        }),
        catchError(()=>{
            Swal.fire({
                title: 'Create Error!',
                icon: 'error',
                text: 'Server Error',
                confirmButtonText: 'OK!'
            })
            return EMPTY;
        })
    ))
   ))
}
import { createSelector, createFeatureSelector } from "@ngrx/store";
import { AccountState } from "../reducers/account.reducer";

export const selectAccount = createFeatureSelector<AccountState>('account');

export const selectAccount_isLoggedIn = createSelector(
    selectAccount,
    (state) => state.isLoggedIn
)

export const selectAccount_currentUser = createSelector(
    selectAccount,
    (state) => state.currentUser
)

export const selectAccount_currentUser_name = createSelector(
    selectAccount,
    (state) => state.currentUser?.name
)
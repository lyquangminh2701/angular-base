import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Account } from '../store/models/account.model';
import { environment } from 'src/environments/environment';
import { of } from 'rxjs';

export interface AuthPayload {
  idUser: number,
  name: string,
  birth: string,
  phone: string,
  email: string,
  role: string,
  token: string,
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private api_url = environment.API_URL;
  constructor(private http: HttpClient) { }

  login(account: Account){
    return this.http.post<AuthPayload>(`${this.api_url}/login`, account);
  }

  logout() {
    localStorage.removeItem('currentUser');
    return of(true);
  }

  authenticate(){
    return this.http.get<AuthPayload>(`${this.api_url}/authenticate`);
  }
}

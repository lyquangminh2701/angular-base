import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../store/models/user.model'; 
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class UsersService {
  API_URL: string = environment.API_URL;
  constructor(private http: HttpClient) {}
  
  getAll(){
    return this.http.get<User[]>(`${this.API_URL}/user?isActive=true`);
  }
  
  getOne(id:number){
    return this.http.get<User>(`${this.API_URL}/user/${id}`);
  }
  
  createOne(user: User){
    return this.http.post<User>(`${this.API_URL}/user`, user);
  }

  deleteOne(id: number){
    console.log("delete service call");
    return this.http.delete<number>(`${this.API_URL}/user/${id}`);
  }

  updateOne(id: number, user: User){
    return this.http.put<User>(`${this.API_URL}/user/${id}`, user);
  }
}